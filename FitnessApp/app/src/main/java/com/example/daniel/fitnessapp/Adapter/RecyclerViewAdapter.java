package com.example.daniel.fitnessapp.Adapter;


import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.daniel.fitnessapp.Interface.ItemClickListener;
import com.example.daniel.fitnessapp.Model.Exercise;
import com.example.daniel.fitnessapp.R;
import com.example.daniel.fitnessapp.ViewExercise;

import org.w3c.dom.Text;

import java.util.List;

class RecylcerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{


    public ImageView image;
    public TextView text;


    private ItemClickListener itemClickListener;


    public RecylcerViewHolder(@NonNull View itemView) {
        super(itemView);
        image = (ImageView)itemView.findViewById(R.id.ex_img);
        text = (TextView) itemView.findViewById(R.id.ex_name);

        itemView.setOnClickListener(this);


    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onClick(View v) {
        itemClickListener.onClick(v,getAdapterPosition());

    }
}

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecylcerViewHolder>{

    private List<Exercise> exerciseList;
    private Context context;


    public RecyclerViewAdapter(List<Exercise> exerciseList, Context context) {
        this.exerciseList = exerciseList;
        this.context = context;
    }

    @NonNull
    @Override
    public RecylcerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View itemView = inflater.inflate(R.layout.item_exercise,viewGroup,false);

        return new RecylcerViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecylcerViewHolder recylcerViewHolder, final int i) {

        recylcerViewHolder.image.setImageResource(exerciseList.get(i).getImage_id());
        recylcerViewHolder.text.setText(exerciseList.get(i).getName());

        recylcerViewHolder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int position) {
                //call to new activity
                Intent intent = new Intent(context,ViewExercise.class);
                intent.putExtra("image_id",exerciseList.get(i).getImage_id());
                intent.putExtra("name",exerciseList.get(i).getName());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return exerciseList.size();
    }
}
