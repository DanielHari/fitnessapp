package com.example.daniel.fitnessapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.daniel.fitnessapp.Adapter.RecyclerViewAdapter;
import com.example.daniel.fitnessapp.Model.Exercise;

import java.util.ArrayList;
import java.util.List;

public class ListExercises extends AppCompatActivity {

    List<Exercise> exerciseList = new ArrayList<>();
    RecyclerView.LayoutManager layoutManager;
    RecyclerView recyclerView;
    RecyclerViewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_exercises);


        initData();

        recyclerView = (RecyclerView)findViewById(R.id.list_ex);
        adapter = new RecyclerViewAdapter(exerciseList,getBaseContext());
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

    }

    private void initData() {

        exerciseList.add(new Exercise(R.drawable.raztezna1,"1. raztezna vaja"));
        exerciseList.add(new Exercise(R.drawable.raztezna2,"2. raztezna vaja"));
        exerciseList.add(new Exercise(R.drawable.raztezna3,"3. raztezna vaja"));
        exerciseList.add(new Exercise(R.drawable.raztezna4,"4. raztezna vaja"));
        exerciseList.add(new Exercise(R.drawable.raztezna5,"5. raztezna vaja"));
        exerciseList.add(new Exercise(R.drawable.raztezna6,"6. raztezna vaja"));
        exerciseList.add(new Exercise(R.drawable.raztezna7,"7. raztezna vaja"));
        exerciseList.add(new Exercise(R.drawable.raztezna8,"8. raztezna vaja"));
        exerciseList.add(new Exercise(R.drawable.raztezna9,"9. raztezna vaja"));
        exerciseList.add(new Exercise(R.drawable.raztezna10,"10. raztezna vaja"));
        exerciseList.add(new Exercise(R.drawable.raztezna11,"11. raztezna vaja"));
        exerciseList.add(new Exercise(R.drawable.raztezna12,"12. raztezna vaja"));
        exerciseList.add(new Exercise(R.drawable.raztezna13,"13. raztezna vaja"));
        exerciseList.add(new Exercise(R.drawable.raztezna14,"14. raztezna vaja"));
        exerciseList.add(new Exercise(R.drawable.raztezna15,"15. raztezna vaja"));
        exerciseList.add(new Exercise(R.drawable.raztezna16,"16. raztezna vaja"));
        exerciseList.add(new Exercise(R.drawable.raztezna17,"17. raztezna vaja"));
        exerciseList.add(new Exercise(R.drawable.raztezna18,"18. raztezna vaja"));
        exerciseList.add(new Exercise(R.drawable.raztezna19,"19. raztezna vaja"));
        exerciseList.add(new Exercise(R.drawable.raztezna20,"20. raztezna vaja"));
        exerciseList.add(new Exercise(R.drawable.raztezna21,"21. raztezna vaja"));
        exerciseList.add(new Exercise(R.drawable.raztezna22,"22. raztezna vaja"));
        exerciseList.add(new Exercise(R.drawable.abs_workout_1,"abs workout example 1"));
        exerciseList.add(new Exercise(R.drawable.abs_workout_2,"abs workout example 2"));
        exerciseList.add(new Exercise(R.drawable.abs_workout_3,"abs workout example 3"));
        exerciseList.add(new Exercise(R.drawable.abs_workout_4,"abs workout example 4"));
        exerciseList.add(new Exercise(R.drawable.barbell_clean,"barbell clean"));
        exerciseList.add(new Exercise(R.drawable.barbell_split_squats,"barbell split squats right"));
        exerciseList.add(new Exercise(R.drawable.barbell_split_squats_l,"barbell split squats left"));
        exerciseList.add(new Exercise(R.drawable.barbell_step_ups,"barbell step ups"));
        exerciseList.add(new Exercise(R.drawable.deadlift,"deadlift"));
        exerciseList.add(new Exercise(R.drawable.home_workout1,"home workout example 1"));
        exerciseList.add(new Exercise(R.drawable.home_workout2,"home workout example 2"));
        exerciseList.add(new Exercise(R.drawable.home_workout3,"home workout example 3"));
        exerciseList.add(new Exercise(R.drawable.home_workout4,"home workout example 4"));
    }
}
