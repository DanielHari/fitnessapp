package com.example.daniel.fitnessapp;

import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.daniel.fitnessapp.Databases.YogaDB;
import com.example.daniel.fitnessapp.Model.Exercise;
import com.example.daniel.fitnessapp.Utils.Common;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import me.zhanghai.android.materialprogressbar.MaterialProgressBar;

public class Daily_Training extends AppCompatActivity {

    Button btnStart;
    ImageView ex_image;
    TextView txtGetReady,txtCountdown,txtTimer,ex_name,rest;
    ProgressBar progressBar;
    LinearLayout layoutGetReady;

    int ex_id=0,limit_time=0;

    List<Exercise> list = new ArrayList<>();

    YogaDB yogaDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily__training);

        initData();

        yogaDB = new YogaDB(this);



        btnStart = (Button)findViewById(R.id.btnStart);
        ex_image = (ImageView)findViewById(R.id.detail_image);
        txtCountdown = (TextView)findViewById(R.id.txtCountDown);
        txtGetReady = (TextView)findViewById(R.id.txtGetReady);
        txtTimer = (TextView)findViewById(R.id.timer);
        ex_name = (TextView)findViewById(R.id.title);
        rest = (TextView)findViewById(R.id.rest);

        layoutGetReady = (LinearLayout)findViewById(R.id.layout_get_Ready);

        progressBar = (MaterialProgressBar)findViewById(R.id.progressBar);

        //Set data
        progressBar.setMax(list.size());

        setExerciseInformation(ex_id);

        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                if(btnStart.getText().toString().toLowerCase().equals("start"))
                {

                    showGetReady();
                    btnStart.setText("končaj");



                }
                else if(btnStart.getText().toString().toLowerCase().equals("končaj"))
                {
                    if(yogaDB.getSettingMode() == 0)
                        exercisesEasyModeCountDown.cancel();
                    else if(yogaDB.getSettingMode() == 1)
                        exercisesMediumModeCountDown.cancel();
                    else if(yogaDB.getSettingMode() == 2)
                        exercisesHardModeCountDown.cancel();

                   // restTimeCountDown.cancel();

                    if(ex_id < list.size())
                    {

                        showRestTime();

                        ex_id++;
                        progressBar.setProgress(ex_id);
                        txtTimer.setText("");

                    }
                    else{

                        showFinished();
                    }

                }
                else{
                if(yogaDB.getSettingMode() == 0)
                    exercisesEasyModeCountDown.cancel();
                else if(yogaDB.getSettingMode() == 1)
                    exercisesMediumModeCountDown.cancel();
                else if(yogaDB.getSettingMode() == 2)
                    exercisesHardModeCountDown.cancel();

                restTimeCountDown.cancel();

                if(ex_id <list.size())
                    setExerciseInformation(ex_id);
                else
                    showFinished();}
            }
        });

    }

    private void showRestTime() {

        ex_image.setVisibility(View.INVISIBLE);
        txtGetReady.setText("POČIVANJE:D");
        restTimeCountDown.start();

        txtTimer.setVisibility(View.INVISIBLE);
        btnStart.setText("PRESKOČI");
        btnStart.setVisibility(View.VISIBLE);
        rest.setVisibility(View.VISIBLE);









    }

    private void showGetReady() {

        ex_image.setVisibility(View.INVISIBLE);

        btnStart.setVisibility(View.INVISIBLE);
        txtTimer.setVisibility(View.INVISIBLE);
        rest.setVisibility(View.INVISIBLE);

        layoutGetReady.setVisibility(View.VISIBLE);
        txtGetReady.setText("PRIPRAVI SE!");
        new CountDownTimer(6000,1000)
        {


            @Override
            public void onTick(long millisUntilFinished) {
                txtCountdown.setText(""+(millisUntilFinished-1000)/1000);
            }

            @Override
            public void onFinish() {
                showExercises();
            }
        }.start();

    }

    private void showExercises() {
        if(ex_id<list.size()) //list size contains all exercises
        {

            ex_image.setVisibility(View.VISIBLE);
            btnStart.setVisibility(View.VISIBLE);
            layoutGetReady.setVisibility(View.INVISIBLE);
            rest.setVisibility(View.INVISIBLE);


            if(yogaDB.getSettingMode() == 0)
                exercisesEasyModeCountDown.start();
            else if(yogaDB.getSettingMode() == 1)
                exercisesMediumModeCountDown.start();
            else if(yogaDB.getSettingMode() == 2)
                exercisesHardModeCountDown.start();



            //Set data
            ex_image.setImageResource(list.get(ex_id).getImage_id());
            ex_name.setText(list.get(ex_id).getName());



        }

        else
            showFinished();
    }

    private void showFinished() {

        ex_image.setVisibility(View.INVISIBLE);
        btnStart.setVisibility(View.INVISIBLE);
        txtTimer.setVisibility(View.INVISIBLE);
        rest.setVisibility(View.INVISIBLE);
        progressBar.setProgress(View.INVISIBLE);

        layoutGetReady.setVisibility(View.VISIBLE);

        txtGetReady.setText("KONČANA VADBA!!");
        txtCountdown.setText("Čestitamo! \n Z vadbo za danes si zaključil!");
        txtCountdown.setTextSize(20);

        //save workout done to DB
        yogaDB .saveDay(""+Calendar.getInstance().getTimeInMillis());



    }

    //countdown
    CountDownTimer exercisesEasyModeCountDown = new CountDownTimer(Common.TIME_LIMIT_EASY,1000) {
        @Override
        public void onTick(long millisUntilFinished) {
            txtTimer.setText(""+(1/1000));
        }

        @Override
        public void onFinish() {
            if(ex_id<list.size()-1)
            {
                ex_id++;
                progressBar.setProgress(ex_id);
                txtTimer.setText("");

                setExerciseInformation(ex_id);
                btnStart.setText("start");

            }
            else{

                showFinished();
            }
        }
    };
    CountDownTimer exercisesMediumModeCountDown = new CountDownTimer(Common.TIME_LIMIT_MEDIUM,1000) {
        @Override
        public void onTick(long millisUntilFinished) {
            txtTimer.setText(""+(1/1000));
        }

        @Override
        public void onFinish() {
            if(ex_id<list.size()-1)
            {
                ex_id++;
                progressBar.setProgress(ex_id);
                txtTimer.setText("");

                setExerciseInformation(ex_id);
                btnStart.setText("start");

            }
            else{
                showFinished();

            }
        }
    };
    CountDownTimer exercisesHardModeCountDown = new CountDownTimer(Common.TIME_LIMIT_HARD,1000) {
        @Override
        public void onTick(long millisUntilFinished) {
            txtTimer.setText(""+(1/1000));
        }

        @Override
        public void onFinish() {
            if(ex_id<list.size()-1)
            {
                ex_id++;
                progressBar.setProgress(ex_id);
                txtTimer.setText("");

                setExerciseInformation(ex_id);
                btnStart.setText("start");

            }
            else{

                showFinished();
            }
        }
    };


    CountDownTimer restTimeCountDown = new CountDownTimer(10000,1000) {
        @Override
        public void onTick(long millisUntilFinished) {
            txtCountdown.setText(""+(1/1000));
        }

        @Override
        public void onFinish() {
            setExerciseInformation(ex_id);
            showRestTime();
        }
    };

    private void setExerciseInformation(int id) {
        ex_image.setImageResource(list.get(id).getImage_id());
        ex_name.setText(list.get(id).getName());
        btnStart.setText("start");

        ex_image.setVisibility(View.VISIBLE);

        btnStart.setVisibility(View.VISIBLE);
        txtTimer.setVisibility(View.INVISIBLE);
        rest.setVisibility(View.INVISIBLE);
        layoutGetReady.setVisibility(View.INVISIBLE);


    }

    private void initData() {

        list.add(new Exercise(R.drawable.raztezna1,"1. raztezna vaja"));
        list.add(new Exercise(R.drawable.raztezna2,"2. raztezna vaja"));
        list.add(new Exercise(R.drawable.raztezna3,"3. raztezna vaja"));
        list.add(new Exercise(R.drawable.raztezna4,"4. raztezna vaja"));
        list.add(new Exercise(R.drawable.raztezna5,"5. raztezna vaja"));
        list.add(new Exercise(R.drawable.raztezna6,"6. raztezna vaja"));
        list.add(new Exercise(R.drawable.raztezna7,"7. raztezna vaja"));
        list.add(new Exercise(R.drawable.raztezna8,"8. raztezna vaja"));
        list.add(new Exercise(R.drawable.raztezna9,"9. raztezna vaja"));
        list.add(new Exercise(R.drawable.raztezna10,"10. raztezna vaja"));
        /*list.add(new Exercise(R.drawable.raztezna11,"11. raztezna vaja"));
        list.add(new Exercise(R.drawable.raztezna12,"12. raztezna vaja"));
        list.add(new Exercise(R.drawable.raztezna13,"13. raztezna vaja"));
        list.add(new Exercise(R.drawable.raztezna14,"14. raztezna vaja"));
        list.add(new Exercise(R.drawable.raztezna15,"15. raztezna vaja"));
        list.add(new Exercise(R.drawable.raztezna16,"16. raztezna vaja"));
        list.add(new Exercise(R.drawable.raztezna17,"17. raztezna vaja"));
        list.add(new Exercise(R.drawable.raztezna18,"18. raztezna vaja"));
        list.add(new Exercise(R.drawable.raztezna19,"19. raztezna vaja"));
        list.add(new Exercise(R.drawable.raztezna20,"20. raztezna vaja"));
        list.add(new Exercise(R.drawable.raztezna21,"21. raztezna vaja"));
        list.add(new Exercise(R.drawable.raztezna22,"22. raztezna vaja"));*/
       /* list.add(new Exercise(R.drawable.abs_workout_1,"abs workout example 1"));
        list.add(new Exercise(R.drawable.abs_workout_2,"abs workout example 2"));
        list.add(new Exercise(R.drawable.abs_workout_3,"abs workout example 3"));
        list.add(new Exercise(R.drawable.abs_workout_4,"abs workout example 4"));
        list.add(new Exercise(R.drawable.barbell_clean,"barbell clean"));
        list.add(new Exercise(R.drawable.barbell_split_squats,"barbell split squats right"));
        list.add(new Exercise(R.drawable.barbell_split_squats_l,"barbell split squats left"));
        list.add(new Exercise(R.drawable.barbell_step_ups,"barbell step ups"));
        list.add(new Exercise(R.drawable.deadlift,"deadlift"));*/
        list.add(new Exercise(R.drawable.home_workout1,"home workout example 1"));
        list.add(new Exercise(R.drawable.home_workout2,"home workout example 2"));
        list.add(new Exercise(R.drawable.home_workout3,"home workout example 3"));
        list.add(new Exercise(R.drawable.home_workout4,"home workout example 4"));
    }
}
